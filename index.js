// start BT1
/**
 * INPUT:
 * - Điểm chuẩn
 * - Khu vực ưu tiên
 * - Đối tượng ưu tiên
 * - Điểm môn 1
 * - Điểm môn 2
 * - Điểm môn 3
 * CÁC BƯỚC XỬ LÝ:
 * - Gán các giá trị Điểm chuẩn, điểm môn 1, điểm môn 2, điểm môn 3 lần lượt cho các biến AdminissScore, Score1st, Score2nd, Score3rd;
 * - Xét các trường hợp không hợp lý: không nhập trường giá trị hoặc số âm -> bắt nhập lại;
 * - Xét trường hợp có ít nhất một điểm bằng 0 -> rớt;
 * - Xét trường hợp tổng điểm < điểm chuẩn -> rớt;
 * - Các trường hợp còn lại -> đậu;
 * OUTPUT:
 * - Kết quả đậu
 * - Kết quả rớt
 */
function calculatorScore(a,b,c){
    var priorityArea=+document.getElementById('priorityArea').value;
    var priorityObject=+document.getElementById('priorityObject').value;
    return a+b+c+priorityArea+priorityObject;
}
function enrollStudent(){
    var admissionScore=+document.getElementById('admissionScore').value;
    var Score1st=+document.getElementById('1stScore').value;
    var Score2nd=+document.getElementById('2ndScore').value;
    var Score3rd=+document.getElementById('3rdScore').value;
    if (document.getElementById('1stScore')==null 
        || document.getElementById('2ndScore') == null
        || document.getElementById('3rdScore') == null
        || document.getElementById('admissionScore') == null
        || Score1st<0 
        || Score2nd<0
        || Score3rd<0
        || admissionScore<0
        || document.getElementById('priorityArea').value == 'Chọn khu vực'
        || document.getElementById('priorityObject').value == 'Chọn đối tượng'){
            return alert(`Vui lòng nhập và chọn dữ liệu phù hợp`);
        }
    if (Score1st==0 || Score2nd==0 || Score3rd==0){
        document.getElementById('enrollStudent').innerHTML=`Thí sinh rớt. Do có điểm bằng 0.`
    } else if (calculatorScore(Score1st,Score2nd,Score3rd)<admissionScore){ 
        document.getElementById('enrollStudent').innerHTML=`Tổng điểm: ${calculatorScore(Score1st,Score2nd,Score3rd)}                                                        <br/> Thí sinh rớt. Điểm tổng kết thấp hơn điểm chuẩn.`
    } else{
        document.getElementById('enrollStudent').innerHTML=`Tổng điểm: ${calculatorScore(Score1st,Score2nd,Score3rd)}                                                            <br/> Chúc mừng bạn đã đậu.`
    }
}
// end BT1

// start BT2
/**
 * INPUT:
 * - Họ tên khách hàng;
 * - Số KW tiêu thụ;
 * CÁC BƯỚC XỬ LÝ:
 * - Gắn giá trị rightPerson ở js từ file html;
 * - Dùng switch case cho từng trường hợp và đưa ra lời chào;
 * OUTPUT:
 * - 
 */
function returnPrice(a){
    if (a<=50){
        return 500;
    } 
    if(a<=100){
        return 650;
    }
    if(a<=200){
        return 850;
    }
    if(a<=350){
        return 1100;
    } 
    if (a>350){        
        return 1300;
    }
}
function electricityBill(){
    var userName1=document.getElementById('userName1').value;
    console.log("🚀 ~ file: index.js:82 ~ electricityBill ~ userName1", userName1)
    var electricityConsumption=document.getElementById('electricityConsumption').value*1;
    console.log("🚀 ~ file: index.js:84 ~ electricityBill ~ electricityConsumption", electricityConsumption)
    var result;
    if (userName1==''
        || electricityConsumption<=0){
            return alert('Vui lòng nhập dữ liệu hợp lý');
    }else{
            if (electricityConsumption<=50){
                result=electricityConsumption*returnPrice(electricityConsumption);
            }else if (electricityConsumption<=100){
                result=500*50+(electricityConsumption-50)*returnPrice(electricityConsumption);
            }else if (electricityConsumption<=200){
                result=500*50+650*50+(electricityConsumption-100)*returnPrice(electricityConsumption);
            }else if (electricityConsumption<=350){
                result=500*50+650*50+850*100+(electricityConsumption-200)*returnPrice(electricityConsumption);
            }else{
                result=500*50+650*50+850*100+1100*150+(electricityConsumption-350)*returnPrice(electricityConsumption);
            }
            document.getElementById('electricityBill').innerHTML=` Khách hàng <b style="color: #FFF000">${userName1.toUpperCase()}</b> cần thanh toán <b style="color: #FFF000>${result.toLocaleString()}</b> VND tiền điện.`
    }
}
// end BT2

// start BT3
/**
 * INPUT:
 * - Họ tên khách hàng;
 * - Tổng thu nhập năm;
 * - Số người phụ thuộc;
 * CÁC BƯỚC XỬ LÝ:
 * - Gán các giá trị userName2, yearIncome, dependentPerson lần lượt cho các trường dữ liệu Họ tên, Tổng thu nhập năm, Số người phụ thuộc;
 * - Xét các trường hợp không hợp lý: 
 *  (+) Họ tên rỗng;
 *  (+) Tổng thu nhập năm <0;
 *  (+) Số người phụ thuộc là số âm hoặc thập phân;
 * => Bắt nhập lại
 * - Tính thu nhập chịu thuế và gắn bằng biến a;
 * - Xét trường hợp thu nhập chịu thuế <= 0 ---> Không cần đóng thuế;
 * - Xét trường hợp thu nhập chịu thế >0 ----> Tính thuế thu nhập cá nhân cần trả theo từng mức quy định;
 * OUTPUT:
 * - Thế thu nhập cá nhân cần phải trả;
 */
function incomeTaxed(yearIncome,dependentPerson){
    return yearIncome-4e+6-dependentPerson*16e+5;
}
function returnTax(a){
    if (a<=60e+6){
        return 0.05;
    } 
    if(a<=120e+6){
        return 0.1;
    }
    if(a<=210e+6){
        return 0.15;
    }
    if(a<=384e+6){
        return 0.2;
    } 
    if (a<=624e+6){        
        return 0.25;
    }
    if (a<=960e+6){        
        return 0.25;
    }
    if (a<=960e+6){        
        return 0.25;
    }
}
function taxBill(){
    var userName2=document.getElementById('userName2').value;
    if (userName2==''){return alert('Vui lòng nhập tên của bạn')}
    var yearIncome=document.getElementById('yearIncome').value*1;
    if (yearIncome<0){return alert('Số tiền thu nhập cần là số dương. Vui lòng nhập lại!')}
    var dependentPerson=document.getElementById('dependentPerson').value*1;
    if (dependentPerson<0 || Number.isInteger(dependentPerson)==false){ return alert('Số người phụ thuộc cần là số NGUYÊN DƯƠNG. Vui lòng nhập lại!')}
    var result;
    var a=incomeTaxed(yearIncome,dependentPerson);
    if (a<=0){
        document.getElementById('taxBill').innerHTML='Poor you! Bạn còn không đủ điều kiện để đóng thuế thu nhập :< Bạn quá nghèo';
    } else {
        if (a<=60e+6){
            result=a*returnTax(a);
        } else if(a<=120e+6){
            result=60e+6*0.05+(a-60e+6)*returnTax(a);
        } else if(a<=210e+6){
            result=60e+6*0.05+60e+6*0.1+(a-120e+6)*returnTax(a);
        } else if(a<=384e+6){
            result=60e+6*0.05+60e+6*0.1+90e+6*0.15+(a-210e+6)*returnTax(a);
        } else if(a<=624e+6){
            result=60e+6*0.05+60e+6*0.1+90e+6*0.15+174e+6*0.2+(a-384e+6)*returnTax(a);
        } else if(a<=960e+6){
            result=60e+6*0.05+60e+6*0.1+90e+6*0.15+174e+6*0.2+240e+6*0.25+(a-624e+6)*returnTax(a);
        } else {
            result=60e+6*0.05+60e+6*0.1+90e+6*0.15+174e+6*0.2+240e+6*0.25+336e+6*0.3+(a-960e+6)*returnTax(a);
        }
        var subResult= Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(result);        
        document.getElementById('taxBill').innerHTML=`<b style="color: #FFF000">${userName2.toUpperCase()}</b> cần đóng thuế thu nhập cá nhân là <b style="color: #FFF000">${subResult}</b>.`;
    }
}
// end BT3

// start BT4
/**
 * INPUT:
 * - Loại khách hàng;
 * - Mã khách hàng;
 * - Số kết nối;
 * - Số kênh cao cấp;
 * CÁC BƯỚC XỬ LÝ:
 * - Xử lý display của #numberOfConnections:
 *  (+) Nếu khách hàng là Cá nhân --> display: none;
 *  (+) Nếu khách hàng là Doanh nghiệp --> display: block;
 * - Gán giá trị cho các biến customerType, userCode, numberofChannel lần lượt từ trường dữ liệu Loại khách hàng, Mã khách hàng, Số kênh cao cấp;
 * - Xét các trường hợp không hợp lý:
 *  (+) Không chọn loại khách hàng;
 *  (+) Không nhập mã khách hàng;
 *  (+) Số kênh cao cấp <= 0 hoặc là số thập phân;
 * => Bắt nhập và chọn lại;
 * - Xét trường hợp khách hàng cá nhân => Tính chi phí cáp;
 * - Xét trường hợp khách hàng doanh nghiệp: 
 *  (+) Gán giá trị cho biến numberOfConnections tương đương số kết nối
 *       (+) Khi numberOfConnections<=10 => Tính chi phí cáp;
 *       (+) Khi numberOfConnections>10  => Tính chi phí cáp; 
 * OUTPUT:
 * - Mã khách hàng + Tiền cáp phải trả
 */
const typeOfCustumer=document.getElementById('customerType');
typeOfCustumer.addEventListener('click',function(){
    if (typeOfCustumer.value=="DN"){
        document.getElementById('numberOfConnections').style.display="block";
    } else {
        document.getElementById('numberOfConnections').style.display="none";
    }
})
function totalFeeCN(numberofChannel){
    return 4.5+20.5+(7.5*numberofChannel);
}
function totalFeeDN(numberofChannel,numberOfConnections){
    if (numberOfConnections<=10){
        return 15+75+(50*numberofChannel);
    } else {
        return 15+(50*numberofChannel)+75+(numberOfConnections-10)*5;
    }
}
function cableBill(){
    var customerType=document.getElementById('customerType').value;
    if (customerType=='Chọn loại khách hàng'){
        return alert(`Vui lòng chọn loại khách hàng`);
    }
    var userCode=document.getElementById('userCode').value;
    if (userCode==''){
        return alert(`Vui lòng nhập mã khách hàng`)
    }
    var numberofChannel=document.getElementById('numberofChannel').value*1;
    if (numberofChannel<=0 || Number.isInteger(numberofChannel)==false){
        return alert(`Số kênh cao cấp không hợp lệ.
        Vui lòng nhập lại, số kênh cao cấp cần là số nguyên dương.`);
    }
    var result;
    if (customerType=='CN'){
        result= totalFeeCN(numberofChannel);
    } else{
        var numberOfConnections=document.getElementById('numberOfConnections').value*1;
        if (numberOfConnections<0 || Number.isInteger(numberOfConnections)==false){
            return alert(` Số kết nối không hợp lệ
            Vui lòng nhập số nguyên dương.`)
        }else{
            result=totalFeeDN(numberofChannel,numberOfConnections);
        }
    }
    var subResult= Intl.NumberFormat('ja-US', { style: 'currency', currency: 'USD' }).format(result);        
        document.getElementById('cableBill').innerHTML=` Mã khách hàng: <b style="color: #FFF000">${userCode}</b>
                                                        <br/>Tiền cáp: <b style="color: #FFF000">${subResult}</b>`
}
// end BT4
